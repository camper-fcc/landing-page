[fCC Build a Product Landing Page Project](https://beta.freecodecamp.org/en/challenges/applied-responsive-web-design-projects/build-a-product-landing-page)

This was for the [product landing page deadline](https://forum.freecodecamp.org/t/build-a-product-landing-page-project-questions-discussions-and-resources-january-2018-cohort/175447?u=camper) of the [January 2018 fCC Cohort](https://forum.freecodecamp.org/t/january-2018-fcc-cohort-starting-soon/165977?u=camper).

[Project](https://camper-fcc.gitlab.io/landing-page/)

[Code](https://gitlab.com/camper-fcc/landing-page)

[fCC Forum Post](https://www.freecodecamp.org/forum/t/product-landing-page-seeking-reviews/176684)

Here are some details of my project:

- Valid [HTML](https://validator.w3.org/nu/?showsource=yes&showoutline=yes&doc=https%3A%2F%2Fcamper-fcc.gitlab.io%2Flanding-page%2F), [CSS](https://jigsaw.w3.org/css-validator/validator?uri=https%3A%2F%2Fcamper-fcc.gitlab.io%2Flanding-page%2F&profile=css3svg&usermedium=all&warning=1&vextwarning=&lang=en) _(The CSS validator doesn't recognize the [justify-items property](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout/Box_Alignment_in_CSS_Grid_Layout#Justifying_Items_on_the_Inline_or_Row_Axis), I guess_), and [WAVE](http://wave.webaim.org/report#/https://camper-fcc.gitlab.io/landing-page/)
- CSS Grid and Flexbox for fully responsive, mobile-first design
- Fixed header
- [No inline styles](https://www.thoughtco.com/avoid-inline-styles-for-css-3466846)
- [No CSS ID selectors for styling](http://oli.jp/2011/ids/)
- Uses [semantic HTML](https://internetingishard.com/html-and-css/semantic-html/) - [fCC Guide on Semantic HTML](https://guide.freecodecamp.org/html/html5-semantic-elements)
- Passes [fCC Beta tests](https://beta.freecodecamp.org/en/challenges/applied-responsive-web-design-projects/build-a-product-landing-page)
- Hosted on [GitLab Pages](https://about.gitlab.com/features/pages/)
- Includes Favicon and [FontAwesome Icons](https://fontawesome.com/)
- Uses [Modern-Normalize.css](https://github.com/sindresorhus/modern-normalize)
- Used [ColorSafe color-picking tool](http://colorsafe.co/) for accessible anchor element colors

Thank you for your time and input! :sunny: